Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1995, Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
License: BSD-2-clause or BSD-3-clause or ISC or public-domain

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: FSFULLR

Files: addr.c
 addr.h
 addrmatch.c
 auth-options.c
 auth-options.h
 auth2-methods.c
 bitmap.c
 bitmap.h
 cipher-chachapoly-libcrypto.c
 cipher-chachapoly.c
 configure.ac
 digest-openssl.c
 digest.h
 krl.c
 krl.h
 moduli.5
 mux.c
 sandbox-darwin.c
 sandbox-null.c
 sandbox-rlimit.c
 sandbox-systrace.c
 sftp-client.c
 sftp-client.h
 sftp-glob.c
 sftp-usergroup.c
 sftp-usergroup.h
 sftp.c
 ssh-sandbox.h
 sshbuf-getput-basic.c
 sshbuf-getput-crypto.c
 sshbuf-io.c
 sshbuf-misc.c
 sshbuf.c
 sshbuf.h
 ssherr.c
 ssherr.h
Copyright: 1999-2008, 2011-2013, 2015, 2018, 2022, 2023, Damien Miller <djm@mindrot.org>
License: ISC

Files: atomicio.c
Copyright: 2006, Damien Miller.
 2005, Anil Madhavapeddy.
 1995, 1999, Theo de Raadt.
License: BSD-2-clause

Files: atomicio.h
Copyright: 2006, Damien Miller.
 1995, 1999, Theo de Raadt.
License: BSD-2-clause

Files: audit-bsm.c
Copyright: 1988-2002, Sun Microsystems, Inc.
License: BSD-2-clause

Files: audit-linux.c
Copyright: 2010, Red Hat, Inc.
License: BSD-2-clause

Files: audit.c
 audit.h
 auth-shadow.c
Copyright: 2004, 2005, Darren Tucker.
License: BSD-2-clause

Files: auth-bsdauth.c
 auth.c
 auth.h
 auth2-hostbased.c
 auth2-kbdint.c
 auth2-none.c
 auth2-passwd.c
 auth2.c
 authfile.c
 authfile.h
 cipher-aes.c
 compat.c
 compat.h
 dispatch.c
 dispatch.h
 fatal.c
 kex-names.c
 kex.c
 kex.h
 kexdh.c
 kexgen.c
 kexmlkem768x25519.c
 kexsntrup761x25519.c
 mac.c
 mac.h
 msg.c
 msg.h
 myproposal.h
 nchan.c
 nchan.ms
 nchan2.ms
 readpass.c
 session.h
 sftp-server.8
 sftp.h
 ssh-dss.c
 ssh-keysign.8
 ssh-keysign.c
 ssh2.h
 sshconnect.h
 sshkey-xmss.c
 sshkey-xmss.h
 sshkey.h
Copyright: 1999-2003, 2013, 2017, 2019, 2023, Markus Friedl.
License: BSD-2-clause

Files: auth-krb5.c
Copyright: 2002, Daniel Kouril.
License: BSD-2-clause

Files: auth-pam.c
Copyright: 2002, Networks Associates Technology, Inc.
License: BSD-2-clause or ISC

Files: auth-pam.h
 defines.h
 entropy.c
 entropy.h
 sftp.1
Copyright: 1999-2003, Damien Miller.
License: BSD-2-clause

Files: auth-passwd.c
 authfd.c
 channels.c
 channels.h
 cipher.c
 cipher.h
 clientloop.c
 clientloop.h
 hostfile.c
 log.c
 match.c
 packet.c
 serverloop.c
 session.c
 ssh-add.1
 ssh-add.c
 ssh-agent.1
 ssh-agent.c
 ssh-keygen.1
 ssh.1
 ssh.c
 ssh_config.5
 sshd.8
 sshd_config.5
 sshlogin.c
 sshtty.c
 ttymodes.c
 ttymodes.h
Copyright: 1995, Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
License: BSD-2-clause

Files: auth-sia.c
 auth-sia.h
Copyright: 2002, Chris Adams.
License: BSD-2-clause

Files: auth2-chall.c
Copyright: 2001, Per Allansson.
 2001, Markus Friedl.
License: BSD-2-clause

Files: auth2-gss.c
 gss-genr.c
 gss-serv-krb5.c
 gss-serv.c
 kexgssc.c
 kexgsss.c
 ssh-gss.h
Copyright: 2001-2009, Simon Wilkinson.
License: BSD-2-clause

Files: auth2-pubkey.c
 auth2-pubkeyfile.c
 kexecdh.c
 sftp-common.c
 sftp-common.h
 ssh-ecdsa.c
 sshconnect2.c
Copyright: 2001, 2008, 2010, Damien Miller.
 2000, 2001, 2019, Markus Friedl.
License: BSD-2-clause

Files: cipher-aesctr.c
 hmac.c
 hmac.h
 sftp-server-main.c
 sftp-server.c
 ssh-ed25519-sk.c
 ssh-pkcs11-helper.8
 ssh-pkcs11-helper.c
 ssh-pkcs11.h
 ssh-sk-helper.8
 ssh_api.c
 ssh_api.h
Copyright: 2000-2004, 2008, 2010, 2012, 2014, 2019, Markus Friedl.
License: ISC

Files: cipher-aesctr.h
 cleanup.c
 ssh-ed25519.c
 ssh-rsa.c
Copyright: 2000, 2003, 2013, 2014, 2019, Markus Friedl <markus@openbsd.org>
License: ISC

Files: cipher-chachapoly.h
Copyright: Damien Miller 2013, <djm@mindrot.org>
License: ISC

Files: configure
Copyright: 1992-1996, 1998-2017, 2020, 2021, Free Software Foundation
License: FSFUL

Files: contrib/aix/buildbff.sh
Copyright: openssh.inventory openssh.post_i openssh.size LICENCE README
License: public-domain

Files: contrib/aix/inventory.sh
Copyright: no-info-found
License: public-domain

Files: contrib/cygwin/ssh-host-config
 contrib/cygwin/ssh-user-config
Copyright: 2000-2014, Red Hat Inc.
License: curl

Files: contrib/findssl.sh
Copyright: no-info-found
License: public-domain

Files: contrib/gnome-ssh-askpass1.c
 contrib/gnome-ssh-askpass2.c
 contrib/gnome-ssh-askpass3.c
Copyright: 1999-2003, Damien Miller.
License: BSD-2-clause

Files: contrib/ssh-copy-id
 contrib/ssh-copy-id.1
Copyright: 1999-2024, Philip Hands <phil@hands.com>
License: BSD-2-clause

Files: debian/*
Copyright: Matthew Vernon, Colin Watson
License: BSD-2-clause

Files: debian/gnome-ssh-askpass.1
Copyright: 2003-2022, Colin Watson <cjwatson@debian.org>
License: ISC

Files: debian/keygen-test/getpid.c
Copyright: 2001-2008, Kees Cook
License: GPL-2+

Files: debian/openssh-client.apport
 debian/openssh-server.apport
Copyright: 2010, Canonical Ltd.
License: GPL-2+

Files: debian/patches/gssapi.patch
Copyright: 2001-2009, Simon Wilkinson.
License: BSD-2-clause

Files: debian/ssh-argv0
Copyright: 2001, Natalie Amery.
License: BSD-2-clause

Files: debian/tests/util
Copyright: 2018, Canonical Ltd.
License: BSD-2-clause

Files: dh.c
 dh.h
Copyright: 2000, Niels Provos.
License: BSD-2-clause

Files: digest-libc.c
Copyright: 2014, Markus Friedl.
 2013, Damien Miller <djm@mindrot.org>
License: ISC

Files: dns.c
 dns.h
Copyright: 2003, Wesley Griffin.
 2003, Jakob Schlyter.
License: BSD-2-clause

Files: groupaccess.c
 groupaccess.h
Copyright: 2001, Kevin Steves.
License: BSD-2-clause

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: kexc25519.c
Copyright: 2019, Markus Friedl.
 2013, Aris Adamantiadis.
 2010, Damien Miller.
License: BSD-2-clause

Files: kexgex.c
 kexgexc.c
 kexgexs.c
 sshd-session.c
 sshd.c
Copyright: 2000-2002, Markus Friedl.
 2000, 2002, Niels Provos.
License: BSD-2-clause

Files: libcrux_mlkem768_sha3.h
Copyright: 2024, Cryspen
License: Expat

Files: loginrec.c
Copyright: 2000, Andre Lucas.
 1998, Todd C. Miller <Todd.Miller@courtesan.com>
 1996, Theo de Raadt <deraadt@openbsd.org>
 1996, Jason Downs
License: BSD-2-clause

Files: loginrec.h
 logintest.c
Copyright: 2000, Andre Lucas.
License: BSD-2-clause

Files: mdoc2man.awk
Copyright: 2003, Peter Stuge <stuge-mdoc2man@cdy.org>
License: ISC

Files: misc.c
Copyright: 2005-2020, Damien Miller.
 2004, Henning Brauer <henning@openbsd.org>
 2000, Markus Friedl.
License: ISC

Files: moduli.c
Copyright: 2000, Niels Provos <provos@citi.umich.edu>
 1996-1998, 2003, William Allen Simpson <wsimpson@greendragon.com>
 1994, Phil Karn <karn@qualcomm.com>
License: BSD-2-clause

Files: monitor.c
 monitor_wrap.c
Copyright: 2002, Niels Provos <provos@citi.umich.edu>
 2002, Markus Friedl <markus@openbsd.org>
License: BSD-2-clause

Files: monitor.h
 monitor_fdpass.c
 monitor_fdpass.h
 monitor_wrap.h
Copyright: 2001, 2002, Niels Provos <provos@citi.umich.edu>
License: BSD-2-clause

Files: openbsd-compat/*
Copyright: 1983, 1989-1993, The Regents of the University of California.
License: BSD-3-clause

Files: openbsd-compat/arc4random.c
 openbsd-compat/arc4random.h
Copyright: 2014, Theo de Raadt <deraadt@openbsd.org>
 2013, Markus Friedl <markus@openbsd.org>
 2008, Damien Miller <djm@mindrot.org>
 1996, David Mazieres <dm@uun.org>
License: ISC

Files: openbsd-compat/arc4random_uniform.c
 openbsd-compat/bsd-getpeereid.c
 openbsd-compat/bsd-misc.c
 openbsd-compat/bsd-misc.h
 openbsd-compat/bsd-openpty.c
 openbsd-compat/bsd-signal.c
 openbsd-compat/bsd-signal.h
 openbsd-compat/libressl-api-compat.c
 openbsd-compat/port-linux.h
Copyright: 1999-2008, 2011-2013, 2015, 2018, 2022, 2023, Damien Miller <djm@mindrot.org>
License: ISC

Files: openbsd-compat/base64.c
 openbsd-compat/base64.h
 openbsd-compat/inet_ntop.c
Copyright: 1996, Internet Software Consortium.
License: ISC

Files: openbsd-compat/basename.c
 openbsd-compat/bsd-closefrom.c
 openbsd-compat/dirname.c
 openbsd-compat/readpassphrase.c
 openbsd-compat/readpassphrase.h
 openbsd-compat/strlcat.c
 openbsd-compat/strlcpy.c
 openbsd-compat/strndup.c
 openbsd-compat/strnlen.c
Copyright: 1997, 1998, 2000-2002, 2004, 2005, 2007, 2010, Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: openbsd-compat/bcrypt_pbkdf.c
Copyright: 2013, Ted Unangst <tedu@openbsd.org>
License: ISC

Files: openbsd-compat/bindresvport.c
Copyright: 2000, Damien Miller.
 1998, Theo de Raadt.
 1996, Jason Downs.
License: BSD-2-clause

Files: openbsd-compat/blf.h
 openbsd-compat/blowfish.c
Copyright: 1997, Niels Provos <provos@citi.umich.edu>
License: BSD-3-clause

Files: openbsd-compat/bsd-asprintf.c
Copyright: 2004, Darren Tucker.
 1997, Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: openbsd-compat/bsd-cygwin_util.c
 openbsd-compat/bsd-cygwin_util.h
Copyright: 2000, 2001, 2011, 2013, Corinna Vinschen <vinschen@redhat.com>
License: BSD-2-clause

Files: openbsd-compat/bsd-err.c
Copyright: 2015, Tim Rice <tim@multitalents.net>
License: BSD-3-clause

Files: openbsd-compat/bsd-flock.c
 openbsd-compat/bsd-getline.c
 openbsd-compat/getopt.h
Copyright: 2000, 2001, 2011, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD or BSD-2-clause

Files: openbsd-compat/bsd-getentropy.c
Copyright: 2013, Markus Friedl <markus@openbsd.org>
 2008, Damien Miller <djm@mindrot.org>
 1996, David Mazieres <dm@uun.org>
License: ISC

Files: openbsd-compat/bsd-malloc.c
 openbsd-compat/bsd-poll.c
 openbsd-compat/bsd-setres_id.c
 openbsd-compat/bsd-setres_id.h
Copyright: 2004, 2005, 2007, 2012, 2017, Darren Tucker (dtucker at zip com au).
License: ISC

Files: openbsd-compat/bsd-nextstep.c
 openbsd-compat/bsd-nextstep.h
 openbsd-compat/bsd-waitpid.c
 openbsd-compat/bsd-waitpid.h
 openbsd-compat/xcrypt.c
Copyright: 2000, 2001, 2003, Ben Lindstrom.
License: BSD-2-clause

Files: openbsd-compat/bsd-poll.h
Copyright: 1996, Theo de Raadt <deraadt@openbsd.org>
License: BSD-2-clause

Files: openbsd-compat/bsd-pselect.c
Copyright: 2021, Darren Tucker (dtucker at dtucker net).
 2001, Markus Friedl.
License: BSD-2-clause

Files: openbsd-compat/bsd-statvfs.c
 openbsd-compat/bsd-statvfs.h
 openbsd-compat/openssl-compat.c
 openbsd-compat/openssl-compat.h
Copyright: 2004-2006, 2008, 2014, 2019, 2020, 2023, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: openbsd-compat/bsd-timegm.c
Copyright: 1997, Kungliga Tekniska Högskolan
License: BSD-3-clause

Files: openbsd-compat/fake-rfc2553.c
 openbsd-compat/fake-rfc2553.h
Copyright: 2000-2003, Damien Miller.
 1999, WIDE Project.
License: BSD-3-clause

Files: openbsd-compat/fmt_scaled.c
Copyright: 2001-2003, Ian F. Darwin.
License: BSD-3-clause

Files: openbsd-compat/fnmatch.c
Copyright: 2011, VMware, Inc.
License: BSD-3-clause or ISC

Files: openbsd-compat/freezero.c
 openbsd-compat/reallocarray.c
 openbsd-compat/recallocarray.c
Copyright: 2004, 2008, 2010, 2011, 2016, 2017, Otto Moerbeek <otto@drijf.net>
License: ISC

Files: openbsd-compat/getopt_long.c
Copyright: 2002, Todd C. Miller <Todd.Miller@courtesan.com>
License: BSD-2-Clause-NetBSD or BSD-2-clause or ISC

Files: openbsd-compat/getrrsetbyname-ldns.c
Copyright: Genoscope <svallet@genoscope.cns.fr>
 2007, Simon Vallet
License: BSD-2-clause or ISC

Files: openbsd-compat/getrrsetbyname.c
 openbsd-compat/getrrsetbyname.h
Copyright: 2001, Jakob Schlyter.
License: BSD-2-clause or ISC

Files: openbsd-compat/md5.c
 openbsd-compat/md5.h
Copyright: no-info-found
License: public-domain

Files: openbsd-compat/memmem.c
Copyright: 2005-2020, Rich Felker, et al.
License: Expat

Files: openbsd-compat/mktemp.c
Copyright: 1997, 2008, 2009, Todd C. Miller <Todd.Miller@courtesan.com>
 1996-1998, 2008, Theo de Raadt <deraadt@openbsd.org>
License: ISC

Files: openbsd-compat/openbsd-compat.h
Copyright: 2003, Ben Lindstrom.
 2002, Tim Rice.
 1999-2003, Damien Miller.
License: BSD-2-clause

Files: openbsd-compat/port-aix.c
 openbsd-compat/port-aix.h
Copyright: 2003-2006, Darren Tucker.
 2001, Gert Doering.
License: BSD-2-clause

Files: openbsd-compat/port-irix.c
 openbsd-compat/port-irix.h
Copyright: 2000, Michael Stone.
 2000, Denis Parker.
License: BSD-2-clause

Files: openbsd-compat/port-linux.c
Copyright: 2006, Damien Miller <djm@mindrot.org>
 2005, Daniel Walsh <dwalsh@redhat.com>
License: ISC

Files: openbsd-compat/port-net.c
 openbsd-compat/port-net.h
Copyright: 2005, Reyk Floeter <reyk@openbsd.org>
License: ISC

Files: openbsd-compat/port-prngd.c
Copyright: 1999-2003, Damien Miller.
License: BSD-2-clause

Files: openbsd-compat/port-solaris.c
 openbsd-compat/port-solaris.h
Copyright: 2006, Chad Mynhier.
License: ISC

Files: openbsd-compat/port-uw.c
Copyright: 2005, Tim Rice.
 2005, The SCO Group.
License: BSD-2-clause

Files: openbsd-compat/port-uw.h
Copyright: 2005, Tim Rice.
License: BSD-2-clause

Files: openbsd-compat/regress/*
Copyright: 2004-2006, 2008, 2014, 2019, 2020, 2023, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: openbsd-compat/regress/snprintftest.c
Copyright: 2005, 2024, Damien Miller <djm@mindrot.org>
 2005, 2020, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: openbsd-compat/regress/strtonumtest.c
Copyright: 2004, 2008, 2010, 2011, 2016, 2017, Otto Moerbeek <otto@drijf.net>
License: ISC

Files: openbsd-compat/rresvport.c
Copyright: 1995, 1996, 1998, Theo de Raadt.
 1983, 1993, 1994, The Regents of the University of California.
License: BSD-3-clause

Files: openbsd-compat/setenv.c
 openbsd-compat/strtoul.c
Copyright: 1987, 1990, Regents of the University of California.
License: BSD-3-clause

Files: openbsd-compat/setproctitle.c
Copyright: 2003, Damien Miller <djm@mindrot.org>
 1988, 1993, The Regents of the University of California.
 1983, 1995-1997, Eric P. Allman
License: BSD-3-clause

Files: openbsd-compat/sha2.c
 openbsd-compat/sha2.h
Copyright: 2000, 2001, Aaron D. Gifford
License: BSD-3-clause

Files: openbsd-compat/strptime.c
Copyright: 1997, 1998, The NetBSD Foundation, Inc.
License: BSD-2-clause

Files: openbsd-compat/strtonum.c
Copyright: 2004, Ted Unangst and Todd Miller
License: ISC

Files: openbsd-compat/sys-tree.h
Copyright: 2001, 2002, Niels Provos <provos@citi.umich.edu>
License: BSD-2-clause

Files: openbsd-compat/timingsafe_bcmp.c
Copyright: 2010, Damien Miller.
License: ISC

Files: openssh.xml.in
Copyright: 2006, Chad Mynhier.
License: ISC

Files: platform-listen.c
 platform-misc.c
 platform-tracing.c
 platform.c
 platform.h
Copyright: 2006, 2016, Darren Tucker.
License: ISC

Files: platform-pledge.c
 sandbox-solaris.c
Copyright: 2015, Joyent, Inc
License: ISC

Files: progressmeter.c
 progressmeter.h
Copyright: 2002, 2003, Nils Nordman.
License: BSD-2-clause

Files: regress/misc/fuzz-harness/ssh-sk-null.cc
Copyright: 2019, Google LLC
License: ISC

Files: regress/misc/sk-dummy/sk-dummy.c
Copyright: 2000, 2003, 2013, 2014, 2019, Markus Friedl <markus@openbsd.org>
License: ISC

Files: regress/mkdtemp.c
Copyright: 2003-2022, Colin Watson <cjwatson@debian.org>
License: ISC

Files: regress/modpipe.c
 regress/setuid-allowed.c
Copyright: 1999-2008, 2011-2013, 2015, 2018, 2022, 2023, Damien Miller <djm@mindrot.org>
License: ISC

Files: regress/netcat.c
Copyright: 2001, Eric Jackson <ericj@monkey.org>
License: BSD-3-clause

Files: regress/timestamp.c
Copyright: 2004-2006, 2008, 2014, 2019, 2020, 2023, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: regress/unittests/test_helper/*
Copyright: 1999-2008, 2011-2013, 2015, 2018, 2022, 2023, Damien Miller <djm@mindrot.org>
License: ISC

Files: regress/unittests/utf8/tests.c
Copyright: no-info-found
License: public-domain

Files: rijndael.c
 rijndael.h
Copyright: no-info-found
License: public-domain

Files: sandbox-capsicum.c
Copyright: 2011, Dag-Erling Smorgrav
License: ISC

Files: sandbox-pledge.c
Copyright: 2015, Theo de Raadt <deraadt@openbsd.org>
License: ISC

Files: sandbox-seccomp-filter.c
Copyright: 2015, 2017, 2019, 2020, 2023, Damien Miller <djm@mindrot.org>
 2012, Will Drewry <wad@dataspill.org>
License: ISC

Files: scp.c
Copyright: 1999, Theo de Raadt.
 1999, Aaron Campbell.
License: BSD-2-clause or BSD-3-clause

Files: sftp-realpath.c
Copyright: 2003, Constantin S. Svintsoff <kostik@iclub.nsu.ru>
License: BSD-3-clause

Files: sk-api.h
 ssh-sk-client.c
 ssh-sk-helper.c
 ssh-sk.c
 ssh-sk.h
 sshsig.c
 sshsig.h
Copyright: 2019, Google LLC
License: ISC

Files: sk-usbhid.c
Copyright: 2020, Pedro Martelletto
 2019, Markus Friedl <markus@openbsd.org>
License: ISC

Files: srclimit.c
Copyright: 2005, 2024, Damien Miller <djm@mindrot.org>
 2005, 2020, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: srclimit.h
 survey.sh.in
Copyright: 2004-2006, 2008, 2014, 2019, 2020, 2023, Darren Tucker <dtucker@openssh.com>
License: ISC

Files: ssh-ecdsa-sk.c
Copyright: 2019, Google Inc.
 2010, Damien Miller.
 2000, Markus Friedl.
License: BSD-2-clause

Files: ssh-null.c
Copyright: 2023, Colin Watson.
License: BSD-2-clause

Files: ssh-pkcs11-client.c
 ssh-pkcs11.c
Copyright: 2014, Pedro Martelletto.
 2010, Markus Friedl.
License: ISC

Files: ssh-xmss.c
Copyright: 2017, Stefan-Lukas Gazdag.
 2017, Markus Friedl.
License: ISC

Files: sshkey.c
Copyright: 2010, 2011, Damien Miller.
 2008, Alexander von Gernler.
 2000, 2001, Markus Friedl.
License: BSD-2-clause

Files: utf8.c
 utf8.h
Copyright: 2016, Ingo Schwarze <schwarze@openbsd.org>
License: ISC

Files: OVERVIEW auth-rhosts.c authfd.h canohost.c canohost.h contrib/suse/rc.sshd hostfile.h includes.h log.h match.h misc.h packet.h pathnames.h pkcs11.h readconf.c readconf.h scp.1 servconf.c servconf.h serverloop.h ssh-keygen.c ssh.h sshconnect.c sshlogin.h sshpty.c sshpty.h uidswap.c uidswap.h umac.c umac.h xmalloc.c xmalloc.h
Copyright:
 1995 Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
 Markus Friedl
 Theo de Raadt
 Niels Provos
 Dug Song
 Aaron Campbell
 Damien Miller
 Kevin Steves
 Daniel Kouril
 Wesley Griffin
 Per Allansson
 Nils Nordman
 Simon Wilkinson
 Ben Lindstrom
 Tim Rice
 Andre Lucas
 Chris Adams
 Corinna Vinschen
 Cray Inc.
 Denis Parker
 Gert Doering
 Jakob Schlyter
 Jason Downs
 Juha Yrjölä
 Michael Stone
 Networks Associates Technology, Inc.
 Solar Designer
 Todd C. Miller
 Wayne Schroeder
 William Jones
 Darren Tucker
 Sun Microsystems
 The SCO Group
 Daniel Walsh
 Red Hat, Inc
 Simon Vallet / Genoscope
 Internet Software Consortium
 Reyk Floeter
 Chad Mynhier
License: OpenSSH
 Tatu Ylonen's original licence is as follows (excluding some terms about
 third-party code which are no longer relevant; see the LICENCE file for
 details):
 .
   As far as I am concerned, the code I have written for this software
   can be used freely for any purpose.  Any derived versions of this
   software must be clearly marked as such, and if the derived work is
   incompatible with the protocol description in the RFC file, it must be
   called by a name other than "ssh" or "Secure Shell".
 .
   Note that any information and cryptographic algorithms used in this
   software are publicly available on the Internet and at any major
   bookstore, scientific library, and patent office worldwide.  More
   information can be found e.g. at "http://www.cs.hut.fi/crypto".
 .
   The legal status of this program is some combination of all these
   permissions and restrictions.  Use only at your own responsibility.
   You will be responsible for any legal consequences yourself; I am not
   making any claims whether possessing or using this is legal or not in
   your country, and I am not taking any responsibility on your behalf.
 .
 Most remaining components of the software are provided under a standard
 2-term BSD licence:
 .
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
   IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
   IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
   NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 Some code is licensed under an ISC-style license, to the following
 copyright holders:
 .
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  .
  THE SOFTWARE IS PROVIDED "AS IS" AND TODD C. MILLER DISCLAIMS ALL
  WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
  OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL TODD C. MILLER BE LIABLE
  FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
  OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: openbsd-compat/bsd-snprintf.c
Copyright: 1995 Patrick Powell
License: Powell-BSD-style
 This code is based on code written by Patrick Powell
 (papowell@astart.com) It may be used for any purpose as long as this
 notice remains intact on all source code distributions

Files: openbsd-compat/sigact.c openbsd-compat/sigact.h
Copyright: 1998, 2000 Free Software Foundation, Inc.
License: Expat-with-advertising-restriction
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, distribute with modifications, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE ABOVE COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name(s) of the above copyright
 holders shall not be used in advertising or otherwise to promote the
 sale, use or other dealings in this Software without prior written
 authorization.

Files: ssh-keyscan.1 ssh-keyscan.c
Copyright: 1995, 1996 David Mazieres <dm@lcs.mit.edu>
License: Mazieres-BSD-style
 Modification and redistribution in source and binary forms is
 permitted provided that due credit is given to the author and the
 OpenBSD project by leaving this copyright notice intact.
